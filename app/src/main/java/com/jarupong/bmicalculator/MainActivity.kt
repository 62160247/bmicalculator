package com.jarupong.bmicalculator

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.jarupong.bmicalculator.databinding.ActivityMainBinding
import java.text.DecimalFormat
import java.text.NumberFormat
import kotlin.math.pow
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.calculateButton.setOnClickListener{ calculateBmi() }
        binding.weightEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)}
        binding.heightEditText.setOnKeyListener { view, keyCode, _ -> handleKeyEvent(view, keyCode)}

    }

    private fun displayBmi(bmi : Double) {
        val df = DecimalFormat("#.##")
        val formatBmi = df.format(bmi)
        binding.bmiResult.text = getString(R.string.bmi, formatBmi)

    }

    private fun calculateBmi (){
        val stringWeight = binding.weightEditText.text.toString()
        val stringHeight = binding.heightEditText.text.toString()
        val weight = stringWeight.toDoubleOrNull()
        val height = stringHeight.toDoubleOrNull()
        if( weight == null || weight == 0.0 && height == null || height == 0.0){
            displayBmi(0.0)
            return
        }
        var bmi = weight / ((height!! / 100).pow(2))
        displayBmi(bmi)
        when (bmi){
            in 1.0..18.49 -> binding.stateResult.text = resources.getString(R.string.underweight)
            in 18.5..24.99 -> binding.stateResult.text = resources.getString(R.string.normal)
            in 25.0..29.99 ,-> binding.stateResult.text = resources.getString(R.string.overweight)
            else -> binding.stateResult.text = resources.getString(R.string.obese)

        }

    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }
}